package com.nagp.flight.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentModel {

    private String city;
    private String code;
    private String checkInTime;

    private String checkOutTime;

    private int noOfRooms;
    private int noOfGuests;

    private int price;
    private String type;
    String paymentStatus;


}
