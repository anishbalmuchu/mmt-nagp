package com.nagp.flight.service;

import com.nagp.flight.model.FlightBookingRequestModel;
import com.nagp.flight.model.FlightBookingResponseModel;
import com.nagp.flight.model.PaymentModel;
import com.nagp.flight.repository.FlightRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class FlightService {

    @Autowired
    private FlightRepo flightRepo;

    public String bookFlight(final FlightBookingRequestModel flightBookingRequestModel) {
        RestTemplate restTemplate = new RestTemplate();
        PaymentModel paymentModel = new PaymentModel();
        paymentModel.setPrice(flightBookingRequestModel.getPrice());
        ResponseEntity<PaymentModel> paymentModelResponseEntity = restTemplate.postForEntity("http://localhost:10102/paymentStatus", paymentModel, PaymentModel.class);
        String paymentStatus = paymentModelResponseEntity.getBody().getPaymentStatus();
        log.info("Payment API has returned the payment status as : " +paymentStatus);
            return paymentStatus;
    }

    public List<FlightBookingResponseModel> fetchFlightDetails(final FlightBookingRequestModel bookingRequestModel) {
        return flightRepo.findAll();

    }

    public String bookflight(PaymentModel paymentModel){

        FlightBookingResponseModel data = flightRepo.findByFlightCode(paymentModel.getCode());
        if(data.getAvailableSeats()<1){
            return "Booking failed";
        }int rooms = data.getAvailableSeats()-1;
        data.setAvailableSeats(rooms);
        flightRepo.save(data);
        return "Booking has been confirmed ";

    }



}
