package com.nagp.flight.controller;


import com.nagp.flight.model.FlightBookingRequestModel;
import com.nagp.flight.model.FlightBookingResponseModel;
import com.nagp.flight.model.PaymentModel;
import com.nagp.flight.service.FlightService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.nagp.flight.constants.FlightConstants.CREATE_FLIGHT_BOOKING;
import static com.nagp.flight.constants.FlightConstants.SEARCH_FLIGHT;

@RestController
@Slf4j
@RequiredArgsConstructor
@CrossOrigin(exposedHeaders = {"Access-Control-Allow-Origin","Access-Control-Allow-Credentials"})
public class FlightController {

    private final FlightService service;

    @RequestMapping(value = CREATE_FLIGHT_BOOKING, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createAccount(@RequestBody final FlightBookingRequestModel flightBookingRequestModel) {
        log.info("Received the request for booking the flight");
        String bookFlight = service.bookFlight(flightBookingRequestModel);
        if (bookFlight.equalsIgnoreCase("true")) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("Booking has been confirmed");
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Booking failed");
        }
    }

    @PostMapping(value = SEARCH_FLIGHT)
    public ResponseEntity<?> fetchFlights(@RequestBody FlightBookingRequestModel bookingRequestModel) {
        log.info("Received request for fetching the flight details");
        List<FlightBookingResponseModel> responseModels = service.fetchFlightDetails(bookingRequestModel);
        return ResponseEntity.ok().body(responseModels);
    }

    @PostMapping(value = "/bookFlights")
    public String bookFlight(@RequestBody PaymentModel paymentModel) {
        log.info("Received request for fetching the flight details");
        return service.bookflight(paymentModel);
    }
}
