package com.nagp.hotel.controller;


import com.nagp.hotel.model.HotelBookingRequestModel;
import com.nagp.hotel.model.HotelBookingResponseModel;
import com.nagp.hotel.model.PaymentModel;
import com.nagp.hotel.service.HotelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.nagp.hotel.constants.HotelConstants.SEARCH_HOTEL;

@RestController
@Slf4j
@RequiredArgsConstructor
public class HotelController {

    private final HotelService hotelService;

//    @RequestMapping(value = CREATE_HOTEL_BOOKING, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> createAccount(@RequestBody final HotelBookingRequestModel hotelBookingRequestModel) {
//        log.info("Received the request for booking the hotel");
//        String bookHotel = hotelService.bookHotel(hotelBookingRequestModel);
//        if (bookHotel.equalsIgnoreCase("true")) {
//            return ResponseEntity.status(HttpStatus.ACCEPTED).body("Booking has been confirmed");
//        } else {
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Booking failed");
//        }
//    }

    @PostMapping(value = SEARCH_HOTEL)
    public ResponseEntity<?> fetchHotels(@RequestBody HotelBookingRequestModel bookingRequestModel) {
        log.info("Received request for fetching the hotel details");
        List<HotelBookingResponseModel> responseModels = hotelService.fetchHotelDetails(bookingRequestModel);
        return ResponseEntity.ok().body(responseModels);
    }

    @PostMapping("/bookRoom")
    public String bookRooms(@RequestBody PaymentModel paymentModel){
      return hotelService.bookRooms(paymentModel);
    }
}
