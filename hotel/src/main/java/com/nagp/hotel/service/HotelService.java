package com.nagp.hotel.service;

import com.nagp.hotel.model.HotelBookingRequestModel;
import com.nagp.hotel.model.HotelBookingResponseModel;
import com.nagp.hotel.model.PaymentModel;
import com.nagp.hotel.repository.HotelRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class HotelService {

    @Autowired
    private HotelRepo hotelRepo;

//    public String bookHotel(final HotelBookingRequestModel hotelBookingRequestModel) {
//        RestTemplate restTemplate = new RestTemplate();
//        PaymentModel paymentModel = new PaymentModel();
//        paymentModel.setPrice(hotelBookingRequestModel.getPrice());
//        ResponseEntity<PaymentModel> paymentModelResponseEntity = restTemplate.postForEntity("http://localhost:10102/paymentStatus", paymentModel, PaymentModel.class);
//        String paymentStatus = paymentModelResponseEntity.getBody().getPaymentStatus();
//        log.info("Payment API has returned the payment status as : " +paymentStatus);
//            return paymentStatus;
//    }

    public List<HotelBookingResponseModel> fetchHotelDetails(final HotelBookingRequestModel bookingRequestModel) {
        return hotelRepo.findAll();
    }


    public String bookRooms(PaymentModel paymentModel){

        HotelBookingResponseModel data = hotelRepo.findByHotelCode(paymentModel.getCode());
        if(data.getAvailableRooms()<1){
          return "Booking failed";
        }int rooms = data.getAvailableRooms()-1;
        data.setAvailableRooms(rooms);
        hotelRepo.save(data);
        return "Booking has been confirmed ";

    }


}
