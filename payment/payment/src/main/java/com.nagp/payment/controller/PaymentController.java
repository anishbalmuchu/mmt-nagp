package com.nagp.payment.controller;


import com.nagp.payment.model.PaymentModel;

import com.netflix.discovery.EurekaClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import static com.nagp.payment.constants.PaymentConstants.FETCH_PAYMENT_STATUS;

@RestController
@Slf4j
@RequiredArgsConstructor
public class PaymentController {

    // private final PaymentService1 paymentService;


//    @PostMapping(value = FETCH_PAYMENT_STATUS)
//    public ResponseEntity<?> fetchHotels(@RequestBody PaymentModel paymentModel) {
//        log.info("Received request for fetching the hotel details");
//        PaymentModel responseModels = paymentService.fetchPaymentDetails(paymentModel);
//        return ResponseEntity.ok().body(responseModels);
//    }
@Autowired
private EurekaClient eurekaClient;
    @PostMapping(value = "/payment")
    public ResponseEntity<?> payment(@RequestBody PaymentModel paymentModel) {
        log.info("Received request for fetching the hotel details");
//        PaymentModel responseModels = paymentService.fetchPaymentDetails(paymentModel);
//        return ResponseEntity.ok().body(responseModels);
        RestTemplate restTemplate = new RestTemplate();
        if (paymentModel.getType().equals("HOTEL")) {
            String baseUrl = eurekaClient.getNextServerFromEureka("hotel", false).getHomePageUrl() + "bookRoom";
            return restTemplate.postForEntity(baseUrl, paymentModel, String.class);

        }
        if (paymentModel.getType().equals("FLIGHT")) {
            String baseUrl = eurekaClient.getNextServerFromEureka("flight", false).getHomePageUrl() + "bookFlights";
            return restTemplate.postForEntity(baseUrl, paymentModel, String.class);
        }
        return ResponseEntity.badRequest().body("Wrong TYPE") ;

    }
}
